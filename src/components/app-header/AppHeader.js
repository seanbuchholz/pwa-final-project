import React, { Component } from 'react';
import './app-header.css';

class AppHeader extends Component {
  render() {
    return (
      <div className="App-header">
        <h1 className="App-title"><span className="fa fa-shield"></span> DW Character Inventory <span className="fa fa-shield"></span></h1>
      </div>
    );
  }
}

export default AppHeader;
