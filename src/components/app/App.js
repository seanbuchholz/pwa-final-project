import _ from 'lodash';
import React, { Component } from 'react';
import './App.css';

import AppHeader from '../app-header/AppHeader';
import CharsList from '../chars-list/CharsList';
import CreateChar from '../create-char/CreateChar';

// Set up default values
const chars = [
  {
    name: 'Aldara',
    charClass: 'Wizard',
    isPc: true
  },
  {
    name: 'Cecil Ironskin',
    charClass: 'Barbarian',
    isPc: true
  }
];

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      chars
    };
  }

  // Load from localStorage if data exists
  componentWillMount() {
    if (JSON.parse(localStorage.getItem('charData'))) {
      this.setState({chars: JSON.parse(localStorage.getItem('charData'))});
    }
  }

  render() {
    return (
      <div className="App">
        <AppHeader />
        <CreateChar chars={this.state.chars} createChar={this.createChar.bind(this)} />
        <CharsList chars={this.state.chars}
                   togglePc={this.togglePc.bind(this)}
                   saveChar={this.saveChar.bind(this)}
                   deleteChar={this.deleteChar.bind(this)}/>
      </div>

    );
  }

  // Toggle state of isPc on click
  togglePc(name){
    const foundChar = _.find(this.state.chars, char => char.name === name);
    foundChar.isPc = !foundChar.isPc;
    this.setState({ chars: this.state.chars });
    localStorage.setItem('charData', JSON.stringify(this.state.chars));
  }

  // Create a new character item
  createChar(charName, charClass){
    this.state.chars.push({
      name: charName,
      charClass: charClass,
      isPc: false
    });
    this.setState({ chars: this.state.chars });
    localStorage.setItem('charData', JSON.stringify(this.state.chars));

  }

  // Save edits to existing character
  saveChar(oldChar, newChar){
    const foundChar = _.find(this.state.chars, char => char.name === oldChar);
    foundChar.name = newChar;
    this.setState({ chars: this.state.chars });
    localStorage.setItem('charData', JSON.stringify(this.state.chars));
  }

  // Delete an existing character
  deleteChar(itemToDelete){
    var confirmDelete = confirm("Are you sure you want to delete this character?");
    if (confirmDelete) {
      _.remove(this.state.chars, char => char.name === itemToDelete);
      this.setState({ chars: this.state.chars });
      localStorage.setItem('charData', JSON.stringify(this.state.chars));
    }
  }

}

export default App;
