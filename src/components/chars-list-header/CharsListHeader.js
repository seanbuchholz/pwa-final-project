import React, { Component } from 'react';
import './chars-list-header.css';

class CharsListHeader extends Component {
  render() {
    return (
      <thead>
        <tr>
          <th className="col-name">Name</th>
          <th className="col-class">Class</th>
          <th className="col-actions">Actions</th>
        </tr>
      </thead>
    );
  }
}

export default CharsListHeader;
