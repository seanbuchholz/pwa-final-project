import React, { Component } from 'react';
import './chars-list-item.css';

class CharsListItem extends Component {

  constructor(props) {
    super(props);

    this.state = {
      isEditing: false,
    };
  }

  // Render Save/Cancel buttons or Edit/Delete buttons based on isEditing state
  renderActionsSection(){
    if (this.state.isEditing) {
      return (
        <td>
          <button onClick={this.onSaveClick.bind(this)} className="btn-action  btn-save"><span className="fa fa-check"></span> Save</button>
          <button onClick={this.onCancelClick.bind(this)} className="btn-action  btn-cxl" title="Cancel"><span className="fa fa-undo"></span> Cancel</button>
        </td>
      );
    }
    return (
      <td>
        <button onClick={this.onEditClick.bind(this)} className="btn-action  btn-edit" title="Edit"><span className="fa fa-pencil"></span> Edit</button>
        <button onClick={this.props.deleteChar.bind(this, this.props.name)} className="btn-action  btn-del"><span className="fa fa-times"></span> Delete</button>
      </td>
    );
  }

  // Render character name or Edit input field based on isEditing state
  renderPcSection(){
    const { name, isPc } = this.props;
    const pcStyle = {
      color: isPc ? '#445963' : '#33c3f0',
      fontWeight: isPc ? '' : '600',
      cursor: 'pointer'
    }

    if (this.state.isEditing) {
      return (
        <td>
          <form onSubmit={this.onSaveClick.bind(this)}>
            <input type="text" defaultValue={name} ref="editInput" />
          </form>
        </td>
      );
    }
    return (
      <td style={pcStyle}
          onClick={this.props.togglePc.bind(this, name)}
          >{name}</td>
    );
  }

  // Render character class
  renderClassSection() {
    const { charClass } = this.props;
    return (
      <td>{charClass}</td>
    );
  }

  render() {
    return (
        <tr>
          {this.renderPcSection()}
          {this.renderClassSection()}
          {this.renderActionsSection()}
        </tr>
    );
  }
  // Toggle state of isEditing
  onEditClick() {
    this.setState({ isEditing: true });
  }

  onCancelClick() {
    this.setState({ isEditing: false });
  }

  // Call saveChar method when Save button or Enter key pressed
  onSaveClick(e) {
    e.preventDefault();

    const oldChar = this.props.name;
    const newChar = this.refs.editInput.value;
    this.props.saveChar(oldChar, newChar);
    this.setState({ isEditing: false });
  }
}

export default CharsListItem;
