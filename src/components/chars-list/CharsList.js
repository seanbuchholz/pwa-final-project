import _ from 'lodash';
import React, { Component } from 'react';

import CharsListHeader from '../chars-list-header/CharsListHeader';
import CharsListItem from '../chars-list-item/CharsListItem';
import './chars-list.css';


class CharsList extends Component {

    // Render list of character items from this.props.chars
    renderItems() {
      const props = _.omit(this.props, 'chars');
      return _.map(this.props.chars, (char, index) => <CharsListItem key={index} {...char} {...props} />)
    }

    render() {
      return (
        <table className="u-full-width">
          <CharsListHeader />
          <tbody>
            {this.renderItems()}
          </tbody>
        </table>
      );
    }
}

export default CharsList;
