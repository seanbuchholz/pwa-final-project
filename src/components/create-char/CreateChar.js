import _ from 'lodash';
import React, { Component } from 'react';
import './create-char.css';

class CreateChar extends Component {
  constructor(props){
    super(props);

    this.state = {
      error: null
    };
  }

  // Display error message
  renderMessage(){
    if (!this.state.error) {
      return <div>Click character name to toggle: <span className="msg-pc">Player</span> | <span className="msg-npc">NPC</span></div>;
    };
    return <div style={{ color: '#a64521' }}>{this.state.error}</div>;
  }

  render() {
    return (
      <form onSubmit={this.handleCreate.bind(this)}>
        <input type="text" placeholder="Name" ref="createNameInput" />
        <input type="text" placeholder="Class" ref="createClassInput" />
        <button className="button-primary">Create</button>
        {this.renderMessage()}
      </form>
    );
  }

  // Click handler for Create button
  handleCreate(e) {
    e.preventDefault();

    const createNameInput = this.refs.createNameInput;
    const createClassInput = this.refs.createClassInput;
    const charName = createNameInput.value;
    const charClass = createClassInput.value;
    // Check field validation
    const validateInput = this.validateInput(charName, charClass);
    if (validateInput) {
      this.setState({ error: validateInput });
      return;
    }
    // Call createChar method and reset fields
    this.setState({ error: null });
    this.props.createChar(charName, charClass);
    this.refs.createNameInput.value = '';
    this.refs.createClassInput.value = '';
  }

  // Ensure input fields are not blank
  validateInput(charName, charClass){
    if (!charName || !charClass) {
      return 'Fields cannot be blank!';
    } else {
      return null;
    }
  }
}

export default CreateChar;
