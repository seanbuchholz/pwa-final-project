import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/app/App';
import './styles/normalize.css';
import './styles/skeleton.css';
import './styles/index.css';

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
